import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TaskComponent } from './task/task.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import { FormulaireComponent } from './formulaire/formulaire.component';
import { TododetailsComponent } from './tododetails/tododetails.component';

const ROUTES:Routes =[
  {path:'task', component :TaskComponent},
  {path:'formulaire', component:FormulaireComponent},
  {path:'',redirectTo:'/task',pathMatch:'full'}
]
@NgModule({
  declarations: [
    AppComponent,
    TaskComponent,
    FormulaireComponent,
    TododetailsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
