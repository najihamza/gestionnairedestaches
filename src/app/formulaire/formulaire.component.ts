import { Component, OnInit } from '@angular/core';
import { TODOS } from '../todos';
@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.css']
})
export class FormulaireComponent implements OnInit {

  priorities : Array<string> = [ "faible" , "moyen","important","tres important"];
  todos = TODOS;

  constructor() { }

  ngOnInit() {
  }
  ajouterTodo(texte: string, desc: string, date: Date, priority: string) {

    let nouveauTodo = {texte: texte, desc: desc, date: date, estFait: false, priority: priority}
    this.todos.push(nouveauTodo);
    this.reinitialiser();
  }

  reinitialiser() {
    this.nouveauTodo = '';
    this.todoDate = null;
    this.priority = 'faible';
    this.todoDesc = '';
  }

}
