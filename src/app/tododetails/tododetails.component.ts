import {Component, Input, OnInit} from '@angular/core';
import { TODOS } from '../todos';
import { Todo } from '../todo';

@Component({
  selector: 'todo-details',
  templateUrl: './tododetails.component.html',
  styleUrls: ['./tododetails.component.css']
})
export class TododetailsComponent implements OnInit {
  @Input()
  todo:Todo;
  edit:Boolean=false;
  textButton:string = "modifier";
  priorities : Array<string> = [ "faible" , "moyen","important","tres important"];

  constructor() { }

  ngOnInit() {
  }

  modifierTodo()
  {
    this.edit = (this.edit ? false : true);
    this.textButton = (this.edit ? 'sauvegarder' : 'modifier' );
  }

}
