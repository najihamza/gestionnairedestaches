import { Component, OnInit } from '@angular/core';
import { TODOS } from '../todos';
import { Todo } from '../todo';
@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  todos = TODOS;
  todoSelectionne:Todo;
  edit:Boolean;

  constructor() { }

  ngOnInit() {
  }

  setClasses(todo: Todo) {
    let classes = {
      faible: todo.priority == 'faible',
      moyenne: todo.priority == 'moyenne',
      haute : todo.priority == 'haute'
    };
    return classes
  }

  selectionnerTodo(todo: Todo){
    this.todoSelectionne=todo;
  }


}
